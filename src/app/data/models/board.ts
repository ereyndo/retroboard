export interface Comment {
  id: number | undefined;
  name: string;
  comment: string;
}

export interface Message {
  id: number | undefined;
  message: string;
  likes: string[];
  comments: Comment[];
}

export interface BoardColumn {
  id?: number;
  name: string;
  messages: Message[];
}

export interface Board {
  id: number | undefined;
  name: string;
  creator: string;
  columns: BoardColumn[];
}
