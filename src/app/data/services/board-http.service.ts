import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, Observable, of} from 'rxjs';
import {Board} from '@data/models/board';

@Injectable({
  providedIn: 'root'
})
export class BoardHttpService {

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(
    private http: HttpClient
  ) {
  }

  getBoards(): Observable<Board[]> {
    return this.http.get<Board[]>('/api/getBoards')
      .pipe(
        catchError(this.handleError<Board[]>([]))
      )
  }

  addBoard(board: Board): Observable<Board[]> {
    return this.http.post<Board[]>('/api/updateBoards', {board, operation: 'add'}, this.httpOptions)
      .pipe(
        catchError(this.handleError<Board[]>())
      );
  }

  updateBoard(board: Board): Observable<Board[]> {
    return this.http.post<Board[]>('/api/updateBoards', {board, operation: 'update'}, this.httpOptions)
      .pipe(
        catchError(this.handleError<Board[]>())
      );
  }

  deleteBoard(board: Board): Observable<Board[]> {
    return this.http.post<Board[]>('/api/updateBoards', {board, operation: 'delete'}, this.httpOptions)
      .pipe(
        catchError(this.handleError<Board[]>())
      );
  }

  private handleError<T>(result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
