import {BoardHttpService} from '@data/services/board-http.service';
import {TestBed} from '@angular/core/testing';
import {HttpClient} from '@angular/common/http';
import {Board} from '@data/models/board';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('BoardHttpService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let boardHttpService: BoardHttpService;
  const mockBoard: Board = {
    id: 0,
    name: 'Alex\'s board',
    creator: 'Alex',
    columns: []
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BoardHttpService]
    });

    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    boardHttpService = TestBed.inject(BoardHttpService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  describe('#getBoards', () => {
    const url = '/api/getBoards';
    const expectedBoards: Board[] = [
      mockBoard
    ];

    it('should get boards', function (done: DoneFn) {
      boardHttpService.getBoards().subscribe({
        next: boards => {
          expect(boards).withContext('expected boards').toEqual(expectedBoards);
          done();
        },
        error: done.fail
      });

      const req = httpTestingController.expectOne(url);
      expect(req.request.method).toEqual('GET');

      req.flush(expectedBoards);
    });

    it('should return an empty array when the server returns 404', (done: DoneFn) => {
      const errorMessage = 'test 404 error';

      boardHttpService.getBoards().subscribe({
        next: value => {
          expect(value).withContext('expected an empty array').toEqual([]);
          done();
        },
        error: done.fail
      });

      const req = httpTestingController.expectOne(url);
      expect(req.request.method).withContext('expected get method').toEqual('GET');

      req.flush(errorMessage, {status: 404, statusText: 'Not Found'});
    });
  });

  describe('#updateBoards', () => {
    const url = '/api/updateBoards';

    it('should add board and return updated boards', (done: DoneFn) => {
      boardHttpService.addBoard(mockBoard).subscribe({
        next: boards => {
          expect(boards).withContext('expected boards').toEqual([mockBoard]);
          done();
        },
        error: done.fail
      });

      const req = httpTestingController.expectOne(url);
      expect(req.request.method).withContext('expected post method').toEqual('POST');
      expect(req.request.body).withContext('expected operation add').toEqual({board: mockBoard, operation: 'add'});

      req.flush([mockBoard]);
    });

    it('should update board and return updated boards', (done: DoneFn) => {
      boardHttpService.updateBoard(mockBoard).subscribe({
        next: boards => {
          expect(boards).withContext('expected boards').toEqual([mockBoard]);
          done();
        },
        error: done.fail
      });

      const req = httpTestingController.expectOne(url);
      expect(req.request.method).withContext('expected post method').toEqual('POST');
      expect(req.request.body).withContext('expected operation update').toEqual({board: mockBoard, operation: 'update'});

      req.flush([mockBoard]);
    });

    it('should delete board and return updated boards', (done: DoneFn) => {
      boardHttpService.deleteBoard(mockBoard).subscribe({
        next: boards => {
          expect(boards).withContext('expected boards').toEqual([]);
          done();
        },
        error: done.fail
      });

      const req = httpTestingController.expectOne(url);
      expect(req.request.method).withContext('expected post method').toEqual('POST');
      expect(req.request.body).withContext('expected operation update').toEqual({board: mockBoard, operation: 'delete'});

      req.flush([]);
    });

    it('should return nothing when the server returns 404', (done: DoneFn) => {
      const errorMessage = 'test 404 error';

      boardHttpService.addBoard(mockBoard).subscribe({
        next: value => {
          expect(value).withContext('expected nothing').toBeUndefined();
          done();
        },
        error: done.fail
      });

      boardHttpService.updateBoard(mockBoard).subscribe({
        next: value => {
          expect(value).withContext('expected nothing').toBeUndefined();
          done();
        },
        error: done.fail
      });

      boardHttpService.deleteBoard(mockBoard).subscribe({
        next: value => {
          expect(value).withContext('expected nothing').toBeUndefined();
          done();
        },
        error: done.fail
      });

      const req = httpTestingController.match(url);
      expect(req.length).withContext('post requests').toEqual(3);

      req.map(el => {
        el.flush(errorMessage, {status: 404, statusText: 'Not Found'});
      })
    });
  });
});
