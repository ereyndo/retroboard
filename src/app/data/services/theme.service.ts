import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private readonly lightThemeActivated$: BehaviorSubject<boolean>;

  constructor() {
    let theme = sessionStorage.getItem('light-theme');
    let doesLightThemeActivated: boolean;
    if (theme === 'true') {
      doesLightThemeActivated = true;
      document.body.classList.toggle('light-theme');
    } else {
      doesLightThemeActivated = false;
    }
    this.lightThemeActivated$ = new BehaviorSubject<boolean>(doesLightThemeActivated);
    this.doesLightThemeActivated()
      .subscribe(doesLightThemeActivated => {
        sessionStorage.setItem('light-theme', '' + doesLightThemeActivated);
      })
  }

  doesLightThemeActivated(): BehaviorSubject<boolean> {
    return this.lightThemeActivated$;
  }

  toggleTheme(): void {
    this.lightThemeActivated$.next(!this.lightThemeActivated$.getValue());
    document.body.classList.toggle('light-theme');
  }
}
