import {APP_INITIALIZER, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {BoardStateService} from '@core/services/board-state.service';
import {HeaderComponent} from '@layout/header/header.component';
import {RegisterDialogComponent} from '@layout/header/register-dialog/register-dialog.component';
import {LogInDialogComponent} from '@layout/header/log-in-dialog/log-in-dialog.component';
import {RouterModule} from '@angular/router';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';

function loadInitialBoardsFactory(provider: BoardStateService) {
  return () => provider.loadInitialBoards();
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RegisterDialogComponent,
    LogInDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NoopAnimationsModule,
    FontAwesomeModule,
    RouterModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    FormsModule

  ],
  providers: [
    BoardStateService,
    {
      provide: APP_INITIALIZER,
      useFactory: loadInitialBoardsFactory,
      deps: [BoardStateService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
