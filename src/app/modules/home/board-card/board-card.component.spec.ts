import {AuthService} from '@core/services/auth.service';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {BoardCardComponent} from '@modules/home/board-card/board-card.component';
import {Board} from '@data/models/board';
import {first} from 'rxjs';
import {RouterLinkDirectiveStub} from '@testing/router-link-directive-stub';
import {DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';

let comp: BoardCardComponent;
let fixture: ComponentFixture<BoardCardComponent>;
let authServiceSpy: jasmine.SpyObj<AuthService>;
let expectedBoard: Board;

describe('BoardCardComponent', () => {
  beforeEach(() => {
    const spy = jasmine.createSpyObj('AuthService', ['getEmail'])

    TestBed.configureTestingModule({
      declarations: [
        BoardCardComponent,
        RouterLinkDirectiveStub
      ],
      providers: [
        {
          provide: AuthService,
          useValue: spy
        }
      ]
    });

    fixture = TestBed.createComponent(BoardCardComponent);
    comp = fixture.componentInstance;

    authServiceSpy = TestBed.inject(AuthService) as jasmine.SpyObj<AuthService>;
    authServiceSpy.getEmail.and.returnValue('Alex@i.ua');

    expectedBoard = {
      id: 0,
      name: 'Alex\'s board',
      creator: 'Alex@i.ua',
      columns: []
    }
    comp.board = expectedBoard;

    fixture.detectChanges();
  });

  it('should display a correct board name', () => {
    const nameElement = fixture.nativeElement.querySelector('.board-name');
    expect(nameElement.textContent).withContext('expected a board name').toBe(comp.board.name);
  });

  it('should display the correct board author', () => {
    const authorElement = fixture.nativeElement.querySelector('.board-author');
    const boardCreator = comp.board.creator.slice(0, comp.board.creator.indexOf('@'));
    expect(authorElement.textContent).withContext('expected an email account name').toBe(boardCreator);
  });

  describe('#routerLink', () => {
    let linkDes: DebugElement[];
    let routerLinks: RouterLinkDirectiveStub[];

    beforeEach(() => {
      linkDes = fixture.debugElement.queryAll(By.directive(RouterLinkDirectiveStub));
      routerLinks = linkDes.map(de => de.injector.get(RouterLinkDirectiveStub));
    });

    it('should have correct route', () => {
      expect(routerLinks.length).withContext('expected one router link').toBe(1);
      expect(routerLinks[0].linkParams).withContext('expected a correct path').toBe(`/board/${comp.board.id}`);
    });

    it('should have ability to click a board link', () => {
      const boardLinkDe: DebugElement = linkDes[0];
      const boardLink: RouterLinkDirectiveStub = boardLinkDe.injector.get(RouterLinkDirectiveStub);

      expect(boardLink.navigatedTo).withContext('shouldn\'t have navigated yet').toBeNull();

      boardLinkDe.nativeElement.click();
      fixture.detectChanges();

      expect(boardLink.navigatedTo).withContext('should navigate').toBe(`/board/${comp.board.id}`);
    });
  });

  describe('#delete-btn', () => {
    let deleteBtnElem: HTMLButtonElement;

    beforeEach(() => {
      deleteBtnElem = fixture.nativeElement.querySelector('.delete-btn');
    });

    it('should raise delete event when clicked', () => {
      let selectedBoard: Board | undefined;

      comp.deleteBoardEvent.pipe(first()).subscribe((board: Board) => selectedBoard = board)

      deleteBtnElem.click();
      expect(selectedBoard).withContext('expected a correct board').toBe(expectedBoard);
    });

    it('should have [ngClass] resolve to "correct-class"', () => {
      expect(deleteBtnElem.classList.contains('disabled')).withContext('expected correct email').toBeFalsy();
    });

    it('should have [ngClass] resolve to "wrong-class"', () => {
      comp.userEmail = 'bychara@i.ua';
      fixture.detectChanges();
      expect(deleteBtnElem.classList.contains('disabled')).withContext('expected wrong email').toBeTruthy();
    });
  });
});
