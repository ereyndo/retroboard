import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Board} from '@data/models/board';
import {AuthService} from '@core/services/auth.service';

@Component({
  selector: 'app-board-card',
  templateUrl: './board-card.component.html',
  styleUrls: ['./board-card.component.scss']
})
export class BoardCardComponent implements OnInit {
  userEmail?: string;

  @Input() board: Board = {
    id: undefined,
    name: '',
    creator: '',
    columns: []
  };

  @Output() deleteBoardEvent = new EventEmitter<Board>();

  constructor(
    private authService: AuthService,
  ) {
  }

  ngOnInit() {
    this.userEmail = this.authService.getEmail();
  }

  delete(): void {
    this.deleteBoardEvent.emit(this.board)
  }
}
