import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  HostListener,
  OnInit,
  Output
} from '@angular/core';
import {MatDialog,} from '@angular/material/dialog';
import {AddBoardDialogComponent} from '@modules/home/add-board-dialog/add-board-dialog.component';
import {Board} from '@data/models/board';
import {AuthService} from '@core/services/auth.service';
import {ThemeService} from '@data/services/theme.service';
import {Subject, takeUntil} from 'rxjs';

@Component({
  selector: 'app-add-new-board',
  templateUrl: './add-new-board.component.html',
  styleUrls: ['./add-new-board.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddNewBoardComponent implements OnInit {
  private ngUnsubscribe = new Subject();

  public name?: string;
  public creator?: string;

  public lightThemeActivated?: boolean;

  @Output() newBoardEvent = new EventEmitter<Board>();

  @HostListener('click') onClick() {
    this.openDialog();
  }

  constructor(
    private dialog: MatDialog,
    private authService: AuthService,
    private themeService: ThemeService,
    private cdr: ChangeDetectorRef,
  ) {
  }

  ngOnInit() {
    this.creator = this.authService.getAuth()?.currentUser()?.email;
    this.themeService.doesLightThemeActivated()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(themeActivated => {
        this.lightThemeActivated = themeActivated;
        this.cdr.markForCheck();
      });
  }

  private addBoard(name: string): void {
    name = name.trim();
    if (!name) {
      return;
    }
    this.newBoardEvent.emit({id: undefined, name, creator: this.creator!, columns: []});
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddBoardDialogComponent, {
      width: '250px',
      data: {name: this.name}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addBoard(result);
      }
    });
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next(null);
    this.ngUnsubscribe.complete();
  }
}
