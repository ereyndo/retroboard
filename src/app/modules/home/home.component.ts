import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Board} from '@data/models/board';
import {BoardStateService} from '@core/services/board-state.service';
import {Subject, takeUntil} from 'rxjs';
import {getId} from '@shared/helpers/id-generator';
import {AuthService} from '@core/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit {
  private ngUnsubscribe = new Subject();
  boards: Board[] = [];
  authStatus?: boolean;
  userEmail?: string;

  constructor(
    private boardsState: BoardStateService,
    private cdr: ChangeDetectorRef,
    private authService: AuthService,
  ) {
  }

  ngOnInit(): void {
    this.boardsState.getBoards()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(boards => {
        this.boards = boards;
        this.cdr.markForCheck();
      });
    this.authStatus = this.authService.getAuthStatus();
    this.userEmail = this.authService.getEmail();
  }

  addBoard(board: Board): void {
    const id = getId(this.boards);
    this.boardsState.addBoard({...board, id});
  }

  deleteBoard(board: Board): void {
    this.boardsState.deleteBoard(board);
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next(null);
    this.ngUnsubscribe.complete();
  }
}
