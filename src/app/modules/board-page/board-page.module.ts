import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BoardPageComponent} from './board-page.component';
import {BoardPageRoutingModule} from './board-page-routing.module';
import {ColumnComponent} from './column/column.component';
import {MessageComponent} from './column/message/message.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {AddColumnDialogComponent} from './add-column-dialog/add-column-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule} from '@angular/forms';
import {AddMessageDialogComponent} from './column/add-message-dialog/add-message-dialog.component';
import {DragDropModule} from '@angular/cdk/drag-drop';

@NgModule({
  declarations: [
    BoardPageComponent,
    ColumnComponent,
    MessageComponent,
    AddColumnDialogComponent,
    AddMessageDialogComponent
  ],
  imports: [
    CommonModule,
    BoardPageRoutingModule,
    FontAwesomeModule,
    FormsModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    DragDropModule,
  ]
})
export class BoardPageModule {
}
