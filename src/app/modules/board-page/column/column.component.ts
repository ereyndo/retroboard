import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import {BoardColumn, Message} from '@data/models/board';
import {faTrashAlt, faGripVertical} from '@fortawesome/free-solid-svg-icons';
import {getId} from '@shared/helpers/id-generator';
import {MatDialog} from '@angular/material/dialog';
import {AddMessageDialogComponent} from '@modules/board-page/column/add-message-dialog/add-message-dialog.component';
import {BoardStateService} from '@core/services/board-state.service';
import {Subject, takeUntil} from 'rxjs';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {cloneDeep} from 'lodash';
import {AuthService} from '@core/services/auth.service';

@Component({
  selector: 'app-column',
  templateUrl: './column.component.html',
  styleUrls: ['./column.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ColumnComponent implements OnInit {
  private ngUnsubscribe = new Subject();
  boardColumn!: BoardColumn;
  boardColumnCopy!: BoardColumn;
  userEmail?: string;

  @Input() ids!: { boardId: number | undefined, columnId: number | undefined };
  @Input() boardCreator!: string;
  @Output() deleteColumnEvent = new EventEmitter<BoardColumn>();
  @Output() updateBoardEvent = new EventEmitter<BoardColumn[]>();

  faTrashAlt = faTrashAlt;
  faGripVertical = faGripVertical;

  constructor(
    private dialog: MatDialog,
    private boardsState: BoardStateService,
    private cdr: ChangeDetectorRef,
    private authService: AuthService,
  ) {
  }

  ngOnInit(): void {
    this.boardsState.getColumn(this.ids.boardId!, this.ids.columnId!)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(column => {
        this.boardColumn = column;
        this.boardColumnCopy = cloneDeep(this.boardColumn);
        this.cdr.markForCheck();
      });
    this.userEmail = this.authService.getEmail();
  }

  addMessage(message: string): void {
    message = message.trim();
    if (!message) {
      return;
    }
    const rightId = getId(this.boardColumn.messages);
    this.boardsState.addMessage(
      this.ids.boardId!,
      this.ids.columnId!,
      {id: rightId, message, likes: [], comments: []}
    );
  }

  deleteMessage(message: Message): void {
    this.boardsState.deleteMessage(
      this.ids.boardId!,
      this.ids.columnId!,
      message
    );
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddMessageDialogComponent, {
      width: '250px',
      data: {name: ''}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addMessage(result);
      }
    });
  }

  private static repairMessageId(column: BoardColumn): BoardColumn {
    column.messages = column.messages.map((m, index) => {
      m.id = index;
      return m;
    });
    return column;
  }

  dropMessage(event: CdkDragDrop<BoardColumn>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data.messages, event.previousIndex, event.currentIndex);
      this.boardColumnCopy = ColumnComponent.repairMessageId(this.boardColumnCopy);
      this.updateBoardEvent.emit([this.boardColumnCopy]);
    } else {
      transferArrayItem(
        event.previousContainer.data.messages,
        event.container.data.messages,
        event.previousIndex,
        event.currentIndex,
      );
      this.boardColumnCopy = ColumnComponent.repairMessageId(this.boardColumnCopy);
      this.updateBoardEvent.emit([event.previousContainer.data, this.boardColumnCopy]);
    }
  }

  isDropDisabled() {
    return this.userEmail !== this.boardCreator;
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next(null);
    this.ngUnsubscribe.complete();
  }
}
