import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import {Message} from '@data/models/board';
import {
  faThumbsUp,
  faComment,
  faTimes,
  faCheckSquare
} from '@fortawesome/free-solid-svg-icons';
import {Subject, takeUntil} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {BoardStateService} from '@core/services/board-state.service';
import {getId} from '@shared/helpers/id-generator';
import {AuthService} from '@core/services/auth.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MessageComponent implements OnInit {
  private ngUnsubscribe = new Subject();
  userName?: string;
  message!: Message;
  commentsEnable: Boolean = false;
  userEmail?: string;

  @Input() ids!: {
    boardId: number | undefined,
    columnId: number | undefined,
    messageId: number | undefined
  };
  @Input() boardCreator!: string;
  @Output() deleteMessageEvent = new EventEmitter<Message>();

  faThumbsUp = faThumbsUp;
  faComment = faComment;
  faTimes = faTimes;
  faCheckSquare = faCheckSquare;

  constructor(
    private dialog: MatDialog,
    private boardsState: BoardStateService,
    private cdr: ChangeDetectorRef,
    private authService: AuthService,
  ) {
  }

  ngOnInit(): void {
    this.boardsState.getMessage(
      this.ids.boardId!,
      this.ids.columnId!,
      this.ids.messageId!
    )
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(message => {
        this.message = message;
        this.cdr.markForCheck();
      });
    this.userName = this.authService.getUserName();
    this.userEmail = this.authService.getEmail();
    const savedCommentsState = sessionStorage.getItem(`${this.ids.boardId}-${ this.ids.columnId}-${this.ids.messageId}`);
    if (savedCommentsState) {
      this.commentsEnable = true;
    }
  }

  toggleLike(): void {
    this.boardsState.toggleLike(
      this.ids.boardId!,
      this.ids.columnId!,
      this.ids.messageId!,
      this.userName!
    );
  }

  toggleComments(): void {
    this.commentsEnable = !this.commentsEnable;
    if (this.commentsEnable) {
      sessionStorage.setItem(`${this.ids.boardId}-${ this.ids.columnId}-${this.ids.messageId}`, 'true');
    } else {
      sessionStorage.removeItem(`${this.ids.boardId}-${ this.ids.columnId}-${this.ids.messageId}`);
    }
  }

  addComment(comment: string): void {
    comment = comment.trim();
    if (!comment) {
      return;
    }
    const rightId = getId(this.message.comments);
    this.boardsState.addComment(
      this.ids.boardId!,
      this.ids.columnId!,
      this.ids.messageId!,
      {
        id: rightId,
        name: this.userName!,
        comment
      }
    );
  }

  checkIfUserLiked(): boolean {
    return !!~this.message.likes.indexOf(this.userName!);
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next(null);
    this.ngUnsubscribe.complete();
  }
}
