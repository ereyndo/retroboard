import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-add-message-dialog',
  templateUrl: './add-message-dialog.component.html',
  styleUrls: ['./add-message-dialog.component.scss']
})
export class AddMessageDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<AddMessageDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { name: string }
  ) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


}
