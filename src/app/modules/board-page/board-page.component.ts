import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Board, BoardColumn} from '@data/models/board';
import {ActivatedRoute} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {AddColumnDialogComponent} from '@modules/board-page/add-column-dialog/add-column-dialog.component';
import {getId} from '@shared/helpers/id-generator';
import {BoardStateService} from '@core/services/board-state.service';
import {Subject, takeUntil} from 'rxjs';
import {AuthService} from '@core/services/auth.service';

@Component({
  selector: 'app-board-page',
  templateUrl: './board-page.component.html',
  styleUrls: ['./board-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BoardPageComponent implements OnInit {
  private ngUnsubscribe = new Subject();
  board: Board = {
    id: Number(this.route.snapshot.paramMap.get('id')),
    name: '',
    creator: '',
    columns: []
  };
  userEmail?: string;

  constructor(
    private boardsState: BoardStateService,
    private cdr: ChangeDetectorRef,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private authService: AuthService,
  ) {
  }

  ngOnInit(): void {
    this.boardsState.getBoard(this.board.id!)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(board => {
        this.board = board;
        this.cdr.markForCheck();
      });
    this.userEmail = this.authService.getEmail();
  }

  addColumn(name: string): void {
    name = name.trim();
    if (!name) {
      return;
    }
    const rightId = getId(this.board.columns);
    this.boardsState.addColumn(this.board.id!, {id: rightId, name, messages: []});
  }

  deleteColumn(column: BoardColumn): void {
    this.boardsState.deleteColumn(this.board.id!, column);
  }

  updateBoard(columns: BoardColumn[]) {
    for (let column of columns) {
      this.board.columns = this.board.columns.map(c => c.id === column.id ? column : c);
    }
    this.boardsState.updateBoard(this.board, this.board.id!);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddColumnDialogComponent, {
      width: '250px',
      data: {name: ''}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addColumn(result);
      }
    });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next(null);
    this.ngUnsubscribe.complete();
  }
}
