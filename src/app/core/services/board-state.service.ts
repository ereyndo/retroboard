import {Injectable} from '@angular/core';
import {Board, BoardColumn, Message, Comment} from '@data/models/board';
import {BehaviorSubject, map, Observable} from 'rxjs';
import {BoardHttpService} from '@data/services/board-http.service';

@Injectable()
export class BoardStateService {

  private boards$!: BehaviorSubject<Board[]>;

  constructor(
    private boardHttpService: BoardHttpService
  ) {
  }

  loadInitialBoards() {
    return new Promise((resolve) => {
      this.boardHttpService.getBoards()
        .subscribe(boards => {
          this.boards$ = new BehaviorSubject<Board[]>(boards);
          resolve(true);
        });
    });
  }

  getBoards(): Observable<Board[]> {
    return this.boards$;
  }

  getBoard(id: number): Observable<Board> {
    return this.boards$.pipe(
      map(boards => boards.find(board => id === board.id)!)
    );
  }

  getColumn(boardId: number, columnId: number): Observable<BoardColumn> {
    return this.boards$.pipe(
      map(boards => boards.find(board => boardId === board.id)!
        .columns.find(column => columnId === column.id)!)
    );
  }

  getMessage(boardId: number, columnId: number, messageId: number): Observable<Message> {
    return this.boards$.pipe(
      map(boards => boards.find(board => boardId === board.id)!
        .columns.find(column => columnId === column.id)!
        .messages.find(message => messageId === message.id)!)
    );
  }

  addBoard(board: Board): void {
    this.boardHttpService.addBoard(board)
      .subscribe(boards => {
        this.boards$.next([
          ...boards
        ]);
      });
  }

  deleteBoard(board: Board): void {
    this.boardHttpService.deleteBoard(board)
      .subscribe(boards => {
        this.boards$.next([
          ...boards
        ]);
      });
  }

  updateBoard(updatedBoard: Board, boardId: number): void {
    this.boardHttpService.updateBoard(updatedBoard)
      .subscribe(boards => {
        this.boards$.next([
          ...boards
        ]);
      });
  }

  addColumn(boardId: number, column: BoardColumn): void {
    const updatedBoard = this.boards$.getValue().find(board => board.id === boardId)!;
    updatedBoard.columns.push(column);
    this.updateBoard(updatedBoard, boardId);
  }

  deleteColumn(boardId: number, column: BoardColumn): void {
    const updatedBoard = this.boards$.getValue().find(board => board.id === boardId)!;
    updatedBoard.columns = updatedBoard.columns.filter(c => c !== column);
    this.updateBoard(updatedBoard, boardId);
  }

  addMessage(boardId: number, columnId: number, message: Message): void {
    const updatedBoard = this.boards$.getValue().find(board => board.id === boardId)!;
    const updatedColumn = updatedBoard.columns.find(c => c.id === columnId)!;
    updatedColumn.messages.push(message);
    this.updateBoard(updatedBoard, boardId);
  }

  deleteMessage(boardId: number, columnId: number, message: Message): void {
    const updatedBoard = this.boards$.getValue().find(board => board.id === boardId)!;
    const updatedColumn = updatedBoard.columns.find(c => c.id === columnId)!;
    updatedColumn.messages = updatedColumn.messages.filter(m => m !== message);
    this.updateBoard(updatedBoard, boardId);
  }

  toggleLike(boardId: number, columnId: number, messageId: number, whoLiked: string): void {
    const updatedBoard = this.boards$.getValue().find(board => board.id === boardId)!;
    const updatedColumn = updatedBoard.columns.find(c => c.id === columnId)!;
    const updatedMessage = updatedColumn.messages.find(m => m.id === messageId)!;
    if (!!~updatedMessage.likes.indexOf(whoLiked)) {
      updatedMessage.likes = updatedMessage.likes.filter(l => l !== whoLiked);
    } else {
      updatedMessage.likes.push(whoLiked);
    }
    this.updateBoard(updatedBoard, boardId);
  }

  addComment(boardId: number, columnId: number, messageId: number, comment: Comment): void {
    const updatedBoard = this.boards$.getValue().find(board => board.id === boardId)!;
    const updatedColumn = updatedBoard.columns.find(c => c.id === columnId)!;
    const updatedMessage = updatedColumn.messages.find(m => m.id === messageId)!;
    updatedMessage.comments.unshift(comment);
    this.updateBoard(updatedBoard, boardId);
  }
}
