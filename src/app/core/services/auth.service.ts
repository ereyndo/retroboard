import {Injectable} from '@angular/core';
import GoTrue from 'gotrue-js';
import {CanActivate, Router} from '@angular/router';
import {BehaviorSubject, catchError, from, map, Observable, of, tap} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService implements CanActivate {

  private auth?: GoTrue = new GoTrue({
    APIUrl: 'https://wow-retro-board.netlify.app/.netlify/identity',
    audience: '',
    setCookie: false,
  });
  private auth$: BehaviorSubject<GoTrue> = new BehaviorSubject<GoTrue>(this.auth!);

  constructor(
    private router: Router,
  ) {
    if (
      !this.getAuthStatus()
      && window.location.hash
      && window.location.hash.indexOf('#confirmation_token=') === 0
    ) {
      const token = window.location.hash.replace('#confirmation_token=', '');
      from(this.auth?.confirm(token, true)!)
        .pipe(
          tap(() => {
            this.renavigate();
            this.auth$.next(this.auth!);
          }),
          catchError(error => of(`Failed with email confirmation: ${error}`))
        ).subscribe();
    }
  }

  private renavigate() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate(['/']);
  }

  signup(login: string, password: string) {
    return from(this.auth?.signup(login, password)!)
      .pipe(
        catchError(error => of(`Bad Promise: ${error}`))
      );
  }

  login(login: string, password: string) {
    return from(this.auth?.login(login, password, true)!)
      .pipe(
        tap(() => {
          this.renavigate();
        }),
        catchError(error => of(`Bad Promise: ${error}`))
      );
  }

  logout() {
    return from(this.auth?.currentUser()?.logout()!)
      .pipe(
        tap(() => {
          this.renavigate();
        }),
        catchError(error => of(`Bad Promise: ${error}`))
      );
  }

  getAuth() {
    return this.auth;
  }

  getAuthStatus(): boolean {
    return !!this.auth?.currentUser();
  }

  getEmail(): string | undefined {
    return this.auth?.currentUser()?.email;
  }

  getUserName(): string | undefined {
    const email = this.getEmail();
    return email?.slice(0, email.indexOf('@'));
  }

  canActivate(): boolean {
    return this.getAuthStatus();
  }

  getUserNameObservable(): Observable<string | undefined> {
    const email = this.getEmail();
    return this.auth$
      .pipe(
        map(auth => {
          const email = auth.currentUser()?.email;
          return email?.slice(0, email.indexOf('@'));
        })
      )
  }

  getAuthStatusObservable(): Observable<boolean> {
    return this.auth$
      .pipe(
        map(auth => {
          return !!auth?.currentUser();
        })
      )
  }
}
