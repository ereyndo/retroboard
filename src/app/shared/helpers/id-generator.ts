interface objectWithIdField {
  id?: number;
}

export const getId = <T extends objectWithIdField>(table: T[]): number => {
  return table.length > 0 ? Math.max(...table.map(t => t.id as number)) + 1 : 0;
}
