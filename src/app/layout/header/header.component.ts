import {Component, OnInit} from '@angular/core';
import {AuthService} from '@core/services/auth.service';
import {faSignInAlt, faSun, faMoon} from '@fortawesome/free-solid-svg-icons';
import {MatDialog} from '@angular/material/dialog';
import {Subject, takeUntil} from 'rxjs';
import {RegisterDialogComponent} from '@layout/header/register-dialog/register-dialog.component';
import {LogInDialogComponent} from '@layout/header/log-in-dialog/log-in-dialog.component';
import {ThemeService} from '@data/services/theme.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  private ngUnsubscribe = new Subject();

  public userName?: string;
  public authStatus?: boolean;

  public lightThemeActivated?: boolean;

  public faSignInAlt = faSignInAlt;
  public faSun = faSun;
  public faMoon = faMoon;

  constructor(
    private authService: AuthService,
    private dialog: MatDialog,
    private themeService: ThemeService,
  ) {
  }

  ngOnInit(): void {
    this.authService.getUserNameObservable()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(userName => {
        this.userName = userName;
      });
    this.authService.getAuthStatusObservable()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(authStatus => {
        this.authStatus = authStatus;
      });
    this.themeService.doesLightThemeActivated()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(themeActivated => {
        this.lightThemeActivated = themeActivated;
      });
  }

  toggleTheme(): void {
    this.themeService.toggleTheme();
  }

  openDialog(value: string): void {
    const dialog: any = value === 'register' ? RegisterDialogComponent : LogInDialogComponent
    const dialogRef = this.dialog.open(
      dialog,
      {
        width: '252px',
        data: {email: '', password: ''},
        panelClass: 'custom-dialog-container'
      });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (value === 'register') {
          this.authService.signup(result[0], result[1]).subscribe();
        } else {
          this.authService.login(result[0], result[1])!
            .subscribe(() => {
              this.authStatus = this.authService.getAuthStatus();
              this.userName = this.authService.getUserName();
            });
        }
      }
    });
  }

  signOut(): void {
    this.authService.logout()!
      .subscribe(() => {
        this.authStatus = this.authService.getAuthStatus();
        this.userName = this.authService.getUserName();
      });
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next(null);
    this.ngUnsubscribe.complete();
  }
}
