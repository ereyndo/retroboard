const getBoardsDocument = require('./getBoardsDocument');

exports.handler = async () => {
  try {
    const boardsDocument = await getBoardsDocument();

    return {
      statusCode: 200,
      body: JSON.stringify(boardsDocument.data.boards)
    };
  } catch (error) {
    console.error('Error: ' + error);
    return {
      statusCode: 400,
      body: JSON.stringify(error)
    };
  }
}
