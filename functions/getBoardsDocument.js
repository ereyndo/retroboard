const faunadb = require('faunadb')

const q = faunadb.query;
const client = new faunadb.Client({
  secret: process.env.FAUNADB_SERVER_SECRET
});

module.exports = async () => {
  try {
    return await client.query(
      q.Get(
        q.Match(
          q.Index('boards-index')
        )
      )
    );
  } catch (error) {
    return error;
  }
}
