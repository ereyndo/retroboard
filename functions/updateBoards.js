const faunadb = require('faunadb');
const getBoardsDocument = require('./getBoardsDocument');

const q = faunadb.query;
const client = new faunadb.Client({
  secret: process.env.FAUNADB_SERVER_SECRET
});

exports.handler = async (event) => {
  try {
    const {board, operation} = JSON.parse(event.body);

    const boardsDocument = await getBoardsDocument();

    let updatedBoardsDocument;
    switch (operation) {
      case 'add':
        updatedBoardsDocument = [...boardsDocument.data.boards, board];
        break;
      case 'delete':
        updatedBoardsDocument = boardsDocument.data.boards.filter(el => el.id !== board.id);
        break;
      case 'update':
        updatedBoardsDocument = boardsDocument.data.boards.map(el => el.id !== board.id ? el : board);
        break;
    }

    const response = await client.query(
      q.Update(
        boardsDocument.ref,
        {
          data: {
            boards: [...updatedBoardsDocument]
          }
        }
      )
    );

    return {
      statusCode: 200,
      body: JSON.stringify(response.data.boards)
    };
  } catch (error) {
    console.error('Error: ' + error);
    return {
      statusCode: 400,
      body: JSON.stringify(error)
    };
  }
}
